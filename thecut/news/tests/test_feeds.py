# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from ..factories import ArticleFactory
from ..feeds import TaggedArticleFeed
from django.http import QueryDict
from django.test import TestCase
from mock import Mock


class TestTaggedArticleFeed(TestCase):

    def test_items_is_not_empty_when_no_tags_are_specified(self):
        article = ArticleFactory()
        feed = TaggedArticleFeed()
        request_stub = Mock()
        request_stub.GET = QueryDict('')
        feed.request = request_stub

        items = feed.items()

        self.assertIn(article, items)

    def test_items_is_empty_when_no_tags_are_specified(self):
        ArticleFactory()
        feed = TaggedArticleFeed()
        feed.allow_unfiltered_queryset = False
        request_stub = Mock()
        request_stub.GET = QueryDict('')
        feed.request = request_stub

        items = feed.items()

        self.assertEqual(0, items.count())

    def test_items_includes_an_article_with_the_given_tag(self):
        article = ArticleFactory(tags='test')
        feed = TaggedArticleFeed()
        request_stub = Mock()
        request_stub.GET = QueryDict('tags=test')
        feed.request = request_stub

        items = feed.items()

        self.assertIn(article, items)

    def test_items_excludes_an_article_without_the_given_tag(self):
        article = ArticleFactory(tags='test')
        feed = TaggedArticleFeed()
        request_stub = Mock()
        request_stub.GET = QueryDict('tags=non-matching-tag')
        feed.request = request_stub

        items = feed.items()

        self.assertNotIn(article, items)
