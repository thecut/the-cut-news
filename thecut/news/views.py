# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import settings
from .models import Article, ArticleCategory
from django.shortcuts import get_object_or_404, redirect
from django.views import generic


class DetailView(generic.DateDetailView):

    allow_future = True

    context_object_name = 'article'

    date_field = 'publish_at'

    model = Article

    month_format = '%m'

    template_name_field = 'template'

    def get_queryset(self, *args, **kwargs):
        queryset = super(DetailView, self).get_queryset(*args, **kwargs)
        return queryset.current_site().active()


class ListView(generic.ListView):

    allow_empty = settings.ALLOW_EMPTY_ARTICLE_LIST

    context_object_name = 'article_list'

    model = Article

    category_model = ArticleCategory

    paginate_by = settings.ARTICLES_PAGINATE_BY

    def get(self, *args, **kwargs):
        page = self.kwargs.get('page', None)
        if page is not None and int(page) < 2:
            category = self.get_category()
            if category:
                return redirect('news:article_list', slug=category.slug,
                                permanent=True)
            else:
                return redirect('news:article_list', permanent=True)
        return super(ListView, self).get(*args, **kwargs)

    def get_category(self):
        if not hasattr(self, '_category'):
            slug = self.kwargs.get('slug', None)
            if slug is not None:
                category = get_object_or_404(
                    self.category_model.objects.active(), slug=slug)
            else:
                category = None
            self._category = category
        return self._category

    def get_context_data(self, *args, **kwargs):
        context_data = super(ListView, self).get_context_data(*args, **kwargs)
        category = self.get_category()
        context_data.setdefault('category', category)
        return context_data

    def get_queryset(self, *args, **kwargs):
        queryset = super(ListView, self).get_queryset(*args, **kwargs)
        category = self.get_category()
        if category:
            queryset = queryset.filter(categories=category)
        return queryset.current_site().active()

    def get_template_names(self, *args, **kwargs):
        template_names = super(ListView, self).get_template_names(*args,
                                                                  **kwargs)
        category = self.get_category()
        if category and category.template:
            template_names = [category.template] + template_names
        return template_names
