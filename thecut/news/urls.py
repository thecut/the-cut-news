# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import feeds, views
from django.conf.urls import include, url


urls = [

    url(r'^$',
        views.ListView.as_view(), name='article_list'),
    url(r'^(?P<page>\d+)$',
        views.ListView.as_view(), name='article_list'),
    url(r'^(?P<slug>[\w-]+)/$',
        views.ListView.as_view(), name='article_list'),
    url(r'^(?P<slug>[\w-]+)/(?P<page>\d+)$',
        views.ListView.as_view(), name='article_list'),

    url(r'^latest\.xml$',
        feeds.LatestArticleFeed(), name='article_feed'),
    url(r'^(?P<slug>[\w-]+)/latest\.xml$',
        feeds.LatestCategoryArticleFeed(), name='article_feed'),

    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[\w-]+)$',
        views.DetailView.as_view(), name='article_detail'),

    # These old url names are kept for backwards-compatibility
    url(r'^(?P<page>\d+)$',
        views.ListView.as_view(), name='paginated_article_list'),
    url(r'^(?P<slug>[\w-]+)/$',
        views.ListView.as_view(), name='category_article_list'),
    url(r'^(?P<slug>[\w-]+)/(?P<page>\d+)$',
        views.ListView.as_view(), name='paginated_category_article_list'),
    url(r'^(?P<slug>[\w-]+)/latest\.xml$',
        feeds.LatestCategoryArticleFeed(), name='category_article_feed'),

]

urlpatterns = [
    url(r'^', include(urls, namespace='news')),
]
