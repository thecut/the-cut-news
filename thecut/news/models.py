# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.conf import settings
from django.db import models
from django.utils import timezone
from thecut.publishing.models import Content, SiteContent
from thecut.publishing.utils import generate_unique_slug

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


class AbstractArticle(SiteContent):
    """Generic article."""

    slug = models.SlugField(unique_for_date='publish_at')

    summary = models.TextField(default='', blank=True)

    class Meta(SiteContent.Meta):
        abstract = True
        ordering = ['-publish_at']
        unique_together = ['site', 'publish_at', 'slug']

    def get_next(self):
        queryset = self.__class__.objects.exclude(pk=self.pk).filter(
            site=self.site, publish_at__gte=self.publish_at).active().order_by(
            'publish_at')[:1]
        return queryset[0] if queryset else None

    def get_previous(self):
        queryset = self.__class__.objects.exclude(pk=self.pk).filter(
            site=self.site, publish_at__lte=self.publish_at).active().order_by(
            '-publish_at')[:1]
        return queryset[0] if queryset else None

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_unique_slug(
                self.title,
                self.__class__.objects.filter(
                    site=self.site,
                    publish_at__year=self.publish_at.year,
                    publish_at__month=self.publish_at.month,
                    publish_at__day=self.publish_at.day))
        return super(AbstractArticle, self).save(*args, **kwargs)


class AbstractArticleCategory(Content):

    slug = models.SlugField(unique=True)

    class Meta(Content.Meta):
        abstract = True
        verbose_name_plural = 'article categories'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_unique_slug(self.title,
                                             self.__class__.objects.all())
        super(AbstractArticleCategory, self).save(*args, **kwargs)


class Article(AbstractArticle):

    categories = models.ManyToManyField('news.ArticleCategory',
                                        related_name='articles', blank=True)

    def get_absolute_url(self):
        # Ensure consistent urls are determined from the server's timezone.

        server_timezone = timezone.pytz.timezone(settings.TIME_ZONE)

        if timezone.is_naive(self.publish_at):
            publish_at = server_timezone.localize(self.publish_at)
        else:
            publish_at = server_timezone.normalize(self.publish_at)

        return reverse('news:article_detail',
                       kwargs={'year': '%04d' % (publish_at.year),
                               'month': '%02d' % (publish_at.month),
                               'day': '%02d' % (publish_at.day),
                               'slug': self.slug})


class ArticleCategory(AbstractArticleCategory):

    def get_absolute_url(self):
        return reverse('news:article_list', kwargs={'slug': self.slug})
