# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import settings
from .models import Article, ArticleCategory
from django.contrib.sitemaps import Sitemap
from django.core.paginator import Paginator

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


class ArticleSitemap(Sitemap):

    model = Article

    def items(self):
        return self.model.objects.current_site().indexable()

    def lastmod(self, obj):
        return obj.updated_at


class ArticleListSitemap(Sitemap):

    model = Article

    def items(self):
        objects = self.model.objects.current_site().active()
        page_size = settings.ARTICLES_PAGINATE_BY
        if objects:
            return Paginator(objects, page_size).page_range if \
                page_size else [1]
        else:
            return [1] if settings.ALLOW_EMPTY_ARTICLE_LIST else []

    def location(self, page):
        if page == 1:
            return reverse('news:article_list')
        else:
            return reverse('news:article_list', kwargs={'page': page})


class CategoryArticleListSitemap(Sitemap):

    model = ArticleCategory

    def items(self):
        categories = self.model.objects.indexable()
        items = []
        for category in categories:
            objects = category.articles.current_site().active()
            page_size = settings.ARTICLES_PAGINATE_BY
            if objects:
                page_range = Paginator(objects, page_size).page_range if \
                    page_size else [1]
            else:
                page_range = [1] if settings.ALLOW_EMPTY_ARTICLE_LIST else []
            items += [(category.slug, page) for page in page_range]
        return items

    def location(self, opts):
        slug, page = opts
        if page == 1:
            return reverse('news:article_list', kwargs={'slug': slug})
        else:
            return reverse('news:article_list', kwargs={'slug': slug,
                                                        'page': page})


sitemaps = {'news_article': ArticleSitemap,
            'news_articlelist': ArticleListSitemap,
            'news_categoryarticlelist': CategoryArticleListSitemap}
