# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.core.management.base import BaseCommand, CommandError
from django.template.defaultfilters import pluralize
from optparse import make_option


class Command(BaseCommand):

    args = '[create]'

    option_list = BaseCommand.option_list + (
        make_option('--categories',
                    action='store',
                    type='int',
                    dest='categories',
                    default=0,
                    help='Number of categories to create.'),

        make_option('--articles',
                    action='store',
                    type='int',
                    dest='articles',
                    default=1,
                    help=('Number of articles to create. If a non-zero '
                          'number of categories is specified, each category '
                          'will have this number of articles.')))

    help = 'create: Create articles and/or categories with fake data'

    def handle(self, command, *args, **options):

        if command not in ['create']:
            raise CommandError('"{0}" is not a valid argument'.format(command))

        if command == 'create':
            from ...factories import (ArticleFakerFactory,
                                      ArticleCategoryFakerFactory)

            number_of_categories = options['categories']
            pluralized_category = pluralize(number_of_categories,
                                            'category,categories')

            number_of_articles = options['articles']
            pluralized_article = pluralize(number_of_articles,
                                           'article,articles')

            for _category in range(number_of_categories):
                category = ArticleCategoryFakerFactory()

                for _article in range(number_of_articles):
                    article = ArticleFakerFactory()
                    article.categories.add(category)

                output = 'Created {0} {1}, each with {2} {3}.'.format(
                    number_of_categories, pluralized_category,
                    number_of_articles, pluralized_article)

            if not number_of_categories:
                ArticleFakerFactory.create_batch(number_of_articles)
                output = 'Created {0} {1}.'.format(number_of_articles,
                                                   pluralized_article)

            try:
                self.stdout.write(output)
            except AttributeError:
                print(output)
