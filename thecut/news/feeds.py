# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Article, ArticleCategory
from django.contrib.sites.models import Site
from django.contrib.syndication.views import Feed
from django.shortcuts import get_object_or_404
from django.utils.feedgenerator import Atom1Feed

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


class LatestArticleFeed(Feed):

    feed_type = Atom1Feed

    model = Article

    subtitle = 'Latest news articles.'

    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model', self.model)
        return super(LatestArticleFeed, self).__init__(*args, **kwargs)

    def link(self):
        return reverse('news:article_list')

    def title(self):
        site = Site.objects.get_current()
        return '{0} - News'.format(site.name)

    def items(self):
        return self.model.objects.current_site().active().order_by(
            '-publish_at')[:10]

    def item_title(self, item):
        return '{0}'.format(item)

    def item_description(self, item):
        return item.content

    def item_pubdate(self, item):
        return item.publish_at


class LatestCategoryArticleFeed(Feed):

    feed_type = Atom1Feed

    category_model = ArticleCategory

    model = Article

    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model', self.model)
        self.category_model = kwargs.pop('category_model', self.category_model)
        return super(LatestCategoryArticleFeed, self).__init__(*args, **kwargs)

    def title(self, obj):
        return 'Latest {0}'.format(obj)

    def link(self, obj):
        return reverse('news:article_list', kwargs={'slug': obj.slug})

    def get_object(self, request, slug):
        return get_object_or_404(self.category_model, slug=slug)

    def items(self, obj):
        return obj.articles.current_site().active().order_by(
            '-publish_at')[:10]

    def item_title(self, item):
        return '{0}'.format(item)

    def item_description(self, item):
        return item.content

    def item_pubdate(self, item):
        return item.publish_at


class TaggedArticleFeed(Feed):

    feed_type = Atom1Feed

    allow_unfiltered_queryset = True

    model = Article

    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model', self.model)
        return super(TaggedArticleFeed, self).__init__(*args, **kwargs)

    def link(self):
        return reverse('article_feed')

    def title(self):
        site = Site.objects.get_current()
        return '{0}'.format(site.name)

    def get_feed(self, obj, request, *args, **kwargs):
        # Ensure we have access to the request later in the lifecycle of the
        # Feed object.
        self.request = request
        return super(TaggedArticleFeed, self).get_feed(obj, request, *args,
                                                       **kwargs)

    def items(self):
        queryset = self.get_queryset()

        tags = self.request.GET.getlist('tags')

        if not tags and self.allow_unfiltered_queryset:
            return queryset

        # If any tags were specified in the querystring, return only articles
        # which have (any of) those tags.
        return queryset.filter(tags__name__in=tags).distinct()

    def get_queryset(self):
        return self.model.objects.current_site().active().order_by(
            '-publish_at')

    def item_title(self, item):
        return '{0}'.format(item)

    def item_description(self, item):
        return item.content

    def item_pubdate(self, item):
        return item.publish_at
