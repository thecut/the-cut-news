# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.conf import settings


ALLOW_EMPTY_ARTICLE_LIST = getattr(settings, 'NEWS_ALLOW_EMPTY_ARTICLE_LIST',
                                   True)

ARTICLES_PAGINATE_BY = getattr(settings, 'NEWS_ARTICLES_PAGINATE_BY', 10)
