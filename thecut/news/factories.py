# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Article, ArticleCategory
from thecut.publishing.factories import ContentFactory, ContentFakerFactory

try:
    from faker import Factory as FakerFactory
except ImportError as error:
    message = '{0}. Try running `pip install fake-factory`.'.format(error)
    raise ImportError(message)

try:
    import factory
except ImportError as error:
    message = '{0}. Try running `pip install factory_boy`.'.format(error)
    raise ImportError(message)


faker = FakerFactory.create()


class ArticleFactory(ContentFactory):

    class Meta(object):
        model = Article


class ArticleFakerFactory(ContentFakerFactory):

    class Meta(object):
        model = Article

    summary = factory.LazyAttribute(lambda o: faker.paragraph())


class ArticleCategoryFactory(ContentFactory):

    class Meta(object):
        model = ArticleCategory


class ArticleCategoryFakerFactory(ContentFakerFactory):

    class Meta(object):
        model = ArticleCategory
