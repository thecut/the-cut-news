django-tagging==0.3.2
django-model-utils==2.0.3
pytz>=2014.4
git+ssh://git@git.thecut.net.au/thecut-authorship@0.5.3
git+ssh://git@git.thecut.net.au/thecut-publishing@0.5.3
